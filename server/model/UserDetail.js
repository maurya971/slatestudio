var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserDetail = new Schema({
  username: { type: String, required: true, unique: true },
  email: { type: String, required: true },
  password: { type: String, required: true }, //hash created from password
  clientId: { type: String, required: true },
  tokenId: String,
  locked: { type: Boolean, default: false },
  status: Boolean,
  type: String, //UserType- admin/user
  createdOn: { type: Date, default: Date.now },
  createdBy: String,
  modifiedOn: { type: Date, default: Date.now },
  modifiedBy: String
});

module.exports = mongoose.model("UserDetail", UserDetail);
