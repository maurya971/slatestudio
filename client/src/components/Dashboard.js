import React from "react";

import { Card, CardBody, CardHeader, Row, Col } from "reactstrap";

import {
  MdInsertChart,
  MdBubbleChart,
  MdPieChart,
  MdShowChart,
  MdPersonPin,
  MdRateReview,
  MdThumbUp,
  MdShare
} from "react-icons/md";

import Page from "../components/Page";
import IncidentList from "./IncidentList";

class DashboardPage extends React.Component {
  render() {
    return (
      <Page className="DashboardPage">
        <Row>
          <Col md="4" sm="12" xs="12">
            <Card>
              <CardHeader>Open Incident</CardHeader>
              <CardBody>
                <IncidentList status="1" />
              </CardBody>
            </Card>
          </Col>

          <Col md="4" sm="12" xs="12">
            <Card>
              <CardHeader>In-Progress Incident</CardHeader>
              <CardBody>
                <IncidentList status="2" />
              </CardBody>
            </Card>
          </Col>
          <Col md="4" sm="12" xs="12">
            <Card>
              <CardHeader>Done Incident</CardHeader>
              <CardBody>
                <IncidentList status="3" />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Page>
    );
  }
}
export default DashboardPage;
