import React from "react";

export default React.createContext({
  token: null,
  username: null,
  clientId: null,
  type: null,
  login: (token, userId, tokenExpiration, clientId) => {},
  logout: () => {}
});
