const { buildSchema } = require("graphql");

module.exports = buildSchema(`
type User {
  _id: ID!
  email: String!
  username: String!
  password: String
}

type AuthData {
    username: String!
    token: String!
    tokenExpiration: Int!
    type: String!
}

input UserInput {
  email: String!
  username: String!
  password: String
  name: String
  clientId: String
}

input LoginInput {
    username: String!
    password: String!
  }

  input IncidentInput {
    title: String!
    description: String
    type: String!
    status: String!
    priority: String!
    assignedTo: String!
  }
  input IncidentUpdate {
    id: String!
    status: String
    actStatus: String
  }
  type Incident {
    _id: ID!
    title: String!
    description: String
    status: String!
    actStatus: String!
    priority: String!
    assignedTo: String!
  }

type RootQuery {
    TempSignups: [User!]!
    users: [User!]!
    getloginDetail(username: String, password: String): AuthData!
    getIncidentList(status: String, assignedTo: String): [Incident!]!
    getIncident(id: String): Incident!
}

type RootMutation {
    createTempSignup(userInput: UserInput): User
    createUserDetail(userInput: UserInput): User
    createIncident(incidentInput: IncidentInput): Incident
    updateIncident(incidentUpdate: IncidentUpdate): Incident
}
schema {
    query: RootQuery
    mutation: RootMutation
}
`);
