const express = require("express");
const app = express();
var path = require("path");
const bodyParser = require("body-parser");
const graphqlHttp = require("express-graphql");
const mongoose = require("mongoose");
const cors = require("cors");
const graphQlSchema = require("./graphql/schema/index");
const graphQlResolvers = require("./graphql/resolvers/index");
const config = require("./config");
let registration = require("./routes/RegistrationRoute");
const isAuth = require("./middleware/is-auth");

let appInfo = {};
appInfo.ipaddress = "127.0.0.1";
appInfo.port = 3000;

//default to a 'localhost' configuration
const connection_string = config.mongoUri;
console.log("INFO: got mongo string " + connection_string);
mongoose.connect(connection_string);
app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "ui")));
///Registering Routes
app.use("/register", registration);

app.use(isAuth);
app.use(
  "/graphql",
  graphqlHttp({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql: true
  })
);

app.listen(appInfo.port, function() {
  console.log(
    "%s: Node server started on %s:%d ...",
    Date(Date.now()),
    appInfo.ipaddress,
    appInfo.port
  );
});
