/**
 * @author ashutoshmaurya
 * This class handle user registration, pre login activity
 */

let express = require("express");
let router = express.Router();
let LogService = require("../service/LogService.js");
let RegistrationService = require("../service/RegistrationService.js");
let ClientService = require("../service/ClientService.js");
let EmailService = require("../service/EmailService.js");
let config = require("../config");

var _sendWelcomeEmail = function(savedUserDetail) {
  let messageObj = {};
  let baseUrl = config.baseUrl;

  messageObj.subject = "Welcome to Slate Studio !!!";
  messageObj.to = savedUserDetail.email;
  messageObj.useTemplate = false;
  messageObj.html = true;

  //messageObj.template = "welcome";
  //messageObj.context = {
  //baseUrl: baseUrl
  //};
  messageObj.message =
    "Thanks for verificaiotn, Please click bellow URL to login " + baseUrl;
  EmailService.sendEmailMessage(messageObj);
};

let _createNewUserLoginDetail = function(user, next) {
  LogService.logInfo(
    "In RegistrationRoute/_createNewUserLoginDetail, creating user login detail"
  );
  let loginDetail = {
    username: user.username,
    email: user.email,
    password: user.password,
    clientId: user.clientId,
    tokenId: "",
    type: "admin",
    createdOn: new Date(),
    createdBy: config.SYSTEMNAME,
    modifiedOn: new Date(),
    modifiedBy: config.SYSTEMNAME
  };
  LogService.logData(
    "In RegistrationRoute/_createNewUserLoginDetail, creating new user with detail",
    loginDetail
  );
  RegistrationService.addLoginDetail(loginDetail, function(
    err,
    userLoginDetail
  ) {
    if (err) {
      LogService.logError(
        "In RegistrationRoute/_createNewUserLoginDetail, Error while creating new userLoginDetail",
        err
      );
      let data = {
        success: false,
        message:
          "Email verified, Something wrong while creating userLoginDetail."
      };
      next(data);
    }
    LogService.logInfo(
      "UserLoginDetail is created with id " + userLoginDetail._id
    );
    let data = {
      success: true,
      message: "Email verified successfully, Please login."
    };
    _sendWelcomeEmail(userLoginDetail);
    next(data);
  });
};

let _addNewClient = function(clientDetail, next) {
  LogService.logInfo("In RegistrationRoute/_addNewClient, creating client");
  let clientDetailToSave = {
    email: clientDetail.email
  };
  LogService.logData(
    "In RegistrationRoute/_addNewClient, creating new client with detail",
    clientDetailToSave
  );
  ClientService.addClient(clientDetailToSave, function(err, userClientDetail) {
    if (err) {
      LogService.logError(
        "In RegistrationRoute/_addNewClient, Error while creating new clientDetail",
        err
      );
      let data = {
        success: false,
        message: "Email verified, Something wrong while creating client."
      };
      next(data);
    }
    LogService.logInfo(
      "Client is created with id " +
        userClientDetail._id +
        " and clientId " +
        userClientDetail.clientId
    );
    clientDetail.clientId = userClientDetail.clientId;
    _createNewUserLoginDetail(clientDetail, next);
  });
};
/**
 * This function verify email and update tempSignupUser to verified
 */
var _verifyTempSignupEmail = function(user, next) {
  var tempSignupUserObj = {
    _id: user._id,
    verified: "Y",
    verificationTime: new Date()
  };
  RegistrationService.updateTempUser(tempSignupUserObj, function(
    err,
    updateduser
  ) {
    if (err) {
      LogService.logInfo("There is something wrong while verifying user");
      let data = {
        success: false,
        message: "There is something wrong while verifying user."
      };
      next(data);
    }
    LogService.logInfo(
      "TtempSignupUser is verified, creating new user login detail"
    );
    _addNewClient(user, next);
    //_createNewUserLoginDetail(user, next);
  });
};

router.get("/completeregistration/:verificationKey", function(req, res) {
  LogService.logInfo(
    "Inside RegistrationRoute/completeregistration" + req.params.verificationKey
  );
  if (!req.params.verificationKey) {
    let data = {
      success: false,
      message: "Wrong URL, Please contact us."
    };
    res.json(data);
  }

  RegistrationService.getTempUserById(req.params.verificationKey, function(
    err,
    tempUser
  ) {
    if (err || !tempUser) {
      let data = {
        success: false,
        message: "Something wrong while getting user, Please contact us."
      };
      res.json(data);
    }

    if (tempUser && tempUser.verified == "Y") {
      let data = {
        success: false,
        message: "URL already Verified. Please Login"
      };
      res.json(data);
    } else if (tempUser && tempUser.verified == "N") {
      _verifyTempSignupEmail(tempUser, function(data) {
        res.json(data);
      });
    }
  });
});

module.exports = router;
