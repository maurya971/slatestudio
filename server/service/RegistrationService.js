let LogService = require("./LogService.js");
let TempSignup = require("../model/TempSignup.js");
let UserDetail = require("../model/UserDetail.js");

/**
 * Return tempSignup user by id
 */
exports.getTempUserById = function(id, next) {
  LogService.logInfo(
    "InSide RegistrationService/getTempUserById, finding user with id " + id
  );
  TempSignup.findOne(
    { _id: id },
    {
      _id: 1,
      username: 1,
      password: 1,
      email: 1,
      verified: 1
    },
    function(err, user) {
      next(null, user);
    }
  );
};

/**
 * This function update tempSignupUser table to verified
 */

exports.updateTempUser = function(userObj, next) {
  var newTempSignup = TempSignup(userObj);
  newTempSignup.verificationTime = new Date();
  var tempSignupToUpdate = newTempSignup.toObject();
  delete tempSignupToUpdate._id;
  TempSignup.update({ _id: userObj._id }, tempSignupToUpdate, function(
    err,
    user
  ) {
    next(null, user);
  });
};

/**
 * Adding logindetail
 */
exports.addLoginDetail = function(userDetailObj, next) {
  var newUserDetail = UserDetail(userDetailObj);
  newUserDetail.save(function(err, savedUserDetail) {
    if (err) {
      next(err);
    } else {
      next(null, savedUserDetail);
    }
  });
};
