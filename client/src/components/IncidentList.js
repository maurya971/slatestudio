import React from "react";
import { Table } from "reactstrap";
import { graphql } from "react-apollo";
import { getIncidentList } from "../queries/incident";
import authContext from "../context/auth-context";
import history from "./History";

class IncidentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      incidentList: []
    };
  }
  static contextType = authContext;
  componentDidMount() {
    if (this.context.username) {
      this.props.getIncidentList
        .refetch({
          status: this.props.status,
          assignedTo: this.context.username
        })
        .then(res => {
          if (res && res.data && res.data.getIncidentList.length > 0) {
            this.setState({
              incidentList: res.data.getIncidentList
            });
          }
        })
        .catch(error => {});
    }
  }
  getIncidentDetail(id) {
    history.push("/incident/" + id);
  }
  render() {
    return (
      <Table responsive hover>
        <tbody>
          {this.state.incidentList.map(incident => (
            <tr
              key={incident._id}
              onClick={() => this.getIncidentDetail(incident._id)}
            >
              <td className="align-middle">
                <p>Title- {incident.title}</p>
                <p>Description- {incident.description}</p>
                <p>Status- {incident.status}</p>
                <p>Assigned To- {incident.assignedTo}</p>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}

export default graphql(getIncidentList, { name: "getIncidentList" })(
  IncidentList
);
