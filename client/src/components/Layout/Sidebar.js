import logo200Image from "../../assets/img/logo/logo_200.png";
import sidebarBgImage from "../../assets/img/sidebar/sidebar-4.jpg";
import SourceLink from "../../components/SourceLink";
import React from "react";
//import FaGithub from "react-icons/fa";
import { FaGithub } from "react-icons/fa";
import { MdDashboard, MdInsertChart, MdWeb } from "react-icons/md";
import { NavLink } from "react-router-dom";
import {
  // UncontrolledTooltip,
  Collapse,
  Nav,
  Navbar,
  NavItem,
  NavLink as BSNavLink
} from "reactstrap";
import bn from "../../utils/bemnames";
import AuthContext from "../../context/auth-context";

const sidebarBackground = {
  backgroundImage: `url("${sidebarBgImage}")`,
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat"
};

const navItems = [
  { to: "/", name: "dashboard", exact: true, Icon: MdDashboard, type: "all" },
  {
    to: "/adduser",
    name: "Add User",
    exact: false,
    Icon: MdWeb,
    type: "admin"
  },
  {
    to: "/addincident",
    name: "Add Incident",
    exact: false,
    Icon: MdInsertChart,
    type: "admin"
  }
];

const bem = bn.create("sidebar");

class Sidebar extends React.Component {
  state = {
    isOpenComponents: true,
    isOpenContents: true,
    isOpenPages: true
  };
  static contextType = AuthContext;
  handleClick = name => () => {
    this.setState(prevState => {
      const isOpen = prevState[`isOpen${name}`];

      return {
        [`isOpen${name}`]: !isOpen
      };
    });
  };

  render() {
    return (
      <aside className={bem.b()} data-image={sidebarBgImage}>
        <div className={bem.e("background")} style={sidebarBackground} />
        <div className={bem.e("content")}>
          <Navbar>
            <SourceLink className="navbar-brand d-flex">
              <span className="text-white">SlateStudio</span>
            </SourceLink>
          </Navbar>
          <Nav vertical>
            <NavItem className={bem.e("nav-item")}>
              <BSNavLink
                id={`navItem-$dashboard-0`}
                className="text-uppercase"
                tag={NavLink}
                to="/"
                activeClassName="active"
                exact={true}
              >
                <MdDashboard className={bem.e("nav-item-icon")} />
                <span className="">dashboard</span>
              </BSNavLink>
            </NavItem>
            {this.context.type == "admin" && (
              <>
                <NavItem className={bem.e("nav-item")}>
                  <BSNavLink
                    id={`navItem-$dashboard-0`}
                    className="text-uppercase"
                    tag={NavLink}
                    to="/adduser"
                    activeClassName="active"
                    exact={true}
                  >
                    <MdWeb className={bem.e("nav-item-icon")} />
                    <span className="">Add User</span>
                  </BSNavLink>
                </NavItem>
                <NavItem className={bem.e("nav-item")}>
                  <BSNavLink
                    id={`navItem-$dashboard-0`}
                    className="text-uppercase"
                    tag={NavLink}
                    to="/addincident"
                    activeClassName="active"
                    exact={true}
                  >
                    <MdInsertChart className={bem.e("nav-item-icon")} />
                    <span className="">Add Incident</span>
                  </BSNavLink>
                </NavItem>
              </>
            )}
          </Nav>
        </div>
      </aside>
    );
  }
}

export default Sidebar;
