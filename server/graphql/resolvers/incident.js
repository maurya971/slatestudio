const Incident = require("../../model/Incident");

module.exports = {
  createIncident: async (args, req) => {
    try {
      const tempIncidentToSave = new Incident({
        title: args.incidentInput.title,
        description: args.incidentInput.description,
        type: args.incidentInput.type,
        status: args.incidentInput.status,
        priority: args.incidentInput.priority,
        assignedTo: args.incidentInput.assignedTo
      });

      const result = await tempIncidentToSave.save();
      console.log(result);
      return { ...result._doc, _id: result.id };
    } catch (err) {
      throw err;
    }
  },
  getIncidentList: async (args, req) => {
    try {
      const tempIncident = await Incident.find({
        status: args.status,
        actStatus: "1",
        assignedTo: args.assignedTo
      });
      return tempIncident.map(user => {
        return {
          ...user._doc
        };
      });
    } catch (err) {
      throw err;
    }
  },
  getIncident: async (args, req) => {
    try {
      const tempIncident = await Incident.findById({
        _id: args.id
      });
      /*return tempIncident.map(user => {
        return {
          ...user._doc
        };
      });*/
      return tempIncident._doc;
    } catch (err) {
      throw err;
    }
  },
  updateIncident: async (args, req) => {
    console.log(args);
    let updateQuery = {};
    if (args.incidentUpdate.status) {
      updateQuery.status = args.incidentUpdate.status;
    }
    if (args.incidentUpdate.actStatus) {
      updateQuery.actStatus = args.incidentUpdate.actStatus;
    }
    console.log("----------");
    console.log(updateQuery);
    try {
      const result = await Incident.findOneAndUpdate(
        { _id: args.incidentUpdate.id },
        updateQuery
      );
      return { ...result._doc, _id: result.id };
    } catch (err) {
      throw err;
    }
  }
};
