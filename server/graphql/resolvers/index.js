const authResolver = require("./auth");
const userResolver = require("./user");
const incidentResolver = require("./incident");

const rootResolver = {
  ...authResolver,
  ...userResolver,
  ...incidentResolver
};

module.exports = rootResolver;
