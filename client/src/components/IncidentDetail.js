import React from "react";

import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert,
  Container
} from "reactstrap";
import { graphql, compose } from "react-apollo";
import { getIncident, updateIncident } from "../queries/incident";
import { getUsers } from "../queries/userQuery";
import authContext from "../context/auth-context";
import history from "./History";

class IncidentDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      status: "",
      priority: "",
      type: "",
      assignedTo: "",
      userList: []
    };

    this.handelOnChange = this.handelOnChange.bind(this);
    this.addIncidentDetail = this.addIncidentDetail.bind(this);
    this.deleteIncident = this.deleteIncident.bind(this);
  }
  static contextType = authContext;
  componentDidMount() {
    this.props.getIncident
      .refetch({
        id: this.props.match.params.id
      })
      .then(res => {
        if (res && res.data && res.data.getIncident) {
          this.setState({
            id: res.data.getIncident._id,
            title: res.data.getIncident.title,
            description: res.data.getIncident.description,
            status: res.data.getIncident.status,
            priority: res.data.getIncident.priority,
            type: res.data.getIncident.type,
            assignedTo: res.data.getIncident.assignedTo
          });
        }
      })
      .catch(error => {});
    //getting useres for dropdown
    this.props.getUsers
      .refetch()
      .then(res => {
        if (res && res.data && res.data.users) {
          this.setState({
            userList: res.data.users
          });
        }
      })
      .catch(error => {});
  }
  handelOnChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  deleteIncident() {
    debugger;
    this.props
      .updateIncident({
        variables: {
          id: this.state.id,
          actStatus: "2"
        }
      })
      .then(res => {
        let message = "Incident created successfully...";
        this.setState({
          addIncidentStatus: "PASS",
          addIncidentMessage: message
        });
        history.push("/");
      })
      .catch(error => {
        let message = "Can not create incident";
        if (
          error &&
          error.graphQLErrors &&
          error.graphQLErrors[0] &&
          error.graphQLErrors[0].message
        ) {
          message = error.graphQLErrors[0].message;
        }
        this.setState({
          addIncidentStatus: "FAIL",
          addIncidentMessage: message
        });
      });
  }
  addIncidentDetail(e) {
    e.preventDefault();
    this.props
      .updateIncident({
        variables: {
          id: this.state.id,
          status: this.state.status
        }
      })
      .then(res => {
        let message = "Incident created successfully...";
        this.setState({
          addIncidentStatus: "PASS",
          addIncidentMessage: message
        });
        history.push("/");
      })
      .catch(error => {
        let message = "Can not create incident";
        if (
          error &&
          error.graphQLErrors &&
          error.graphQLErrors[0] &&
          error.graphQLErrors[0].message
        ) {
          message = error.graphQLErrors[0].message;
        }
        this.setState({
          addIncidentStatus: "FAIL",
          addIncidentMessage: message
        });
      });
  }

  render() {
    let userList = this.state.userList;
    let userItems = userList.map(user => (
      <option key={user._id} value={user.username}>
        {user.username}
      </option>
    ));
    return (
      <Container>
        <Row>
          <Col lg={8} md={8} sm={12} xs={12}>
            <Card>
              <CardHeader>Incident Detail</CardHeader>
              <CardBody>
                {this.state.addIncidentStatus === "FAIL" && (
                  <Alert color="danger">{this.state.addIncidentMessage}</Alert>
                )}
                {this.state.addIncidentStatus === "PASS" && (
                  <Alert color="success">{this.state.addIncidentMessage}</Alert>
                )}

                <Form onSubmit={this.addIncidentDetail}>
                  <FormGroup>
                    <Label for="addIncTitle">Incident Title</Label>
                    <Input
                      type="text"
                      name="title"
                      placeholder="Sort description of incident"
                      value={this.state.title}
                      id="addIncTitle"
                      onChange={this.handelOnChange}
                      readOnly
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="addIncDesc">Incident Description</Label>
                    <Input
                      type="textarea"
                      name="description"
                      id="addIncDesc"
                      onChange={this.handelOnChange}
                      placeholder="Incident detailed description"
                      rows="2"
                      autoComplete="off"
                      value={this.state.description}
                      readOnly
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="addIncType">Incident Type</Label>
                    <Input
                      type="select"
                      name="type"
                      id="addIncType"
                      value={this.state.type}
                      onChange={this.handelOnChange}
                      readOnly
                      disabled
                    >
                      <option value="">--Select Type--</option>
                      <option value="1">Incident</option>
                      <option value="2">Question</option>
                      <option value="3">Problem</option>
                      <option value="4">Task</option>
                    </Input>
                  </FormGroup>
                  <FormGroup>
                    <Label for="addIncPriority">Priority</Label>
                    <Input
                      type="select"
                      name="priority"
                      id="addIncPriority"
                      value={this.state.priority}
                      onChange={this.handelOnChange}
                      readOnly
                      disabled
                    >
                      <option value="">--Select priority--</option>
                      <option value="1">Low</option>
                      <option value="2">Mediun</option>
                      <option value="3">High</option>
                    </Input>
                  </FormGroup>
                  <FormGroup>
                    <Label for="myProfileDisplayName">Assign to</Label>
                    <Input
                      type="select"
                      name="assignedTo"
                      id="addIncPriority"
                      value={this.state.assignedTo}
                      onChange={this.handelOnChange}
                      required
                      disabled
                    >
                      <option value="">--Select User--</option>
                      {userItems}
                    </Input>
                  </FormGroup>
                  <FormGroup>
                    <Label for="addIncStatus">Status</Label>
                    <Input
                      type="select"
                      name="status"
                      id="addIncStatus"
                      value={this.state.status}
                      onChange={this.handelOnChange}
                    >
                      <option value="">--Select Status--</option>
                      <option value="1">Open</option>
                      <option value="2">Progress</option>
                      <option value="3">Done</option>
                      <option value="4">Closeed</option>
                    </Input>
                  </FormGroup>

                  <FormGroup check row className="text-center">
                    <Button color="success">Update</Button>
                    {this.context.type == "admin" && (
                      <Button onClick={this.deleteIncident}>Delete</Button>
                    )}
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

//export default graphql(incidentInput, { name: "getUsers" })(AddIncident);

export default compose(
  graphql(getIncident, { name: "getIncident" }),
  graphql(getUsers, { name: "getUsers" }),
  graphql(updateIncident, { name: "updateIncident" })
)(IncidentDetail);
