var mongoose = require("mongoose");
var Schema = mongoose.Schema;
//var autoIncrement = require("mongoose-auto-increment");
//autoIncrement.initialize(mongoose.connection);

var Client = new Schema({
  name: String,
  phone: String,
  email: String,
  website: String,
  //clientId: { type: Number, default: 1, unique: true },
  clientId: { type: Number, unique: true },
  createdOn: { type: Date, default: Date.now },
  createdBy: String,
  modifiedOn: { type: Date, default: Date.now }
});

/*ClientSchema.plugin(autoIncrement.plugin, {
  model: "Client",
  field: "clientId",
  startAt: 1,
  incrementBy: 1
});*/
module.exports = mongoose.model("Client", Client);
