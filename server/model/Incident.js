const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Incidence = new Schema({
  title: { type: String, required: true },
  description: { type: String },
  status: { type: String, default: "open" },
  priority: String,
  assignedTo: String,
  actStatus: { type: String, default: "1" }, //active status 1= active 2=deleted
  createdOn: { type: Date, default: Date.now },
  createdBy: String,
  modifiedOn: { type: Date, default: Date.now },
  modifiedBy: String
});

module.exports = mongoose.model("Incidence", Incidence);
