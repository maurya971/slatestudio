import React from "react";

import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert,
  Container
} from "reactstrap";
import { graphql } from "react-apollo";
import { createUserDetail } from "../queries/queries";
import authContext from "../context/auth-context";
import history from "./History";

class AddUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      username: "",
      profileUpdateStatus: null,
      addUserMessage: ""
    };

    this.handelOnChange = this.handelOnChange.bind(this);
    this.addUserDetail = this.addUserDetail.bind(this);
  }
  static contextType = authContext;

  handelOnChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  addUserDetail(e) {
    e.preventDefault();
    debugger;
    this.props
      .createUserDetail({
        variables: {
          name: this.state.name,
          username: this.state.username,
          email: this.state.email,
          clientId: this.context.clientId
        }
      })
      .then(res => {
        let message = "User created successfully...";
        this.setState({
          profileUpdateStatus: "PASS",
          addUserMessage: message
        });
        history.push("/");
      })
      .catch(error => {
        let message = "user created successfully";
        if (
          error &&
          error.graphQLErrors &&
          error.graphQLErrors[0] &&
          error.graphQLErrors[0].message
        ) {
          message = error.graphQLErrors[0].message;
        }
        this.setState({
          profileUpdateStatus: "FAIL",
          addUserMessage: message
        });
      });
  }

  render() {
    return (
      <Container>
        <Row>
          <Col lg={8} md={8} sm={12} xs={12}>
            <Card>
              <CardHeader>Add User</CardHeader>
              <CardBody>
                {this.state.profileUpdateStatus === "FAIL" && (
                  <Alert color="danger">{this.state.addUserMessage}</Alert>
                )}
                {this.state.profileUpdateStatus === "PASS" && (
                  <Alert color="success">{this.state.addUserMessage}</Alert>
                )}

                <Form onSubmit={this.addUserDetail}>
                  <FormGroup>
                    <Label for="myProfileDisplayName">Username</Label>
                    <Input
                      type="text"
                      name="username"
                      placeholder="username must be unique"
                      value={this.state.username}
                      id="userInfoUsername"
                      onChange={this.handelOnChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="myProfileName">Name</Label>
                    <Input
                      type="text"
                      name="name"
                      value={this.state.name}
                      placeholder="Your full name"
                      id="userInfoName"
                      onChange={this.handelOnChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="myProfilePhoneNumber">Email</Label>
                    <Input
                      type="email"
                      name="email"
                      id="userInfoEmail"
                      value={this.state.email}
                      placeholder="user`s Email id"
                      onChange={this.handelOnChange}
                    />
                  </FormGroup>
                  <FormGroup check row className="text-center">
                    <Button>Add User</Button>
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default graphql(createUserDetail, { name: "createUserDetail" })(AddUser);
