import PropTypes from "prop-types";
import React from "react";
import { Alert } from "reactstrap";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { graphql, compose } from "react-apollo";
import { createTempSignup } from "../queries/queries";

// const doRegisterMutation = gql`
//   mutation {
//     doRegisterMutation(username: "", email: "", password: "")
//   }
// `;

class AuthForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      signupEmail: "",
      signupUsername: "",
      signupPassword: "",
      signupConfirmPassword: "",
      loginUsername: "",
      loginPassword: "",
      registrationSuccess: null,
      registrationMessage: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  /*componentWillReceiveProps(nextProps) {
    if (
      nextProps &&
      nextProps.loginResponse &&
      !nextProps.loginSuccess == "pass"
    ) {
      this.setState({ loginFailed: true });
    } else if (
      nextProps &&
      nextProps.register &&
      nextProps.registrationSuccess == "pass"
    ) {
      this.setState({
        name: "",
        signupEmail: "",
        signupUsername: "",
        signupPassword: "",
        signupConfirmPassword: ""
      });
    } else if (
      nextProps &&
      nextProps.loginResponse &&
      nextProps.loginSuccess == "pass" &&
      nextProps &&
      nextProps.isAuthenticated &&
      nextProps.userDetail
    ) {
      history.push("/");
    }
  }*/

  get isLogin() {
    return this.props.authState === STATE_LOGIN;
  }

  get isSignup() {
    return this.props.authState === STATE_SIGNUP;
  }
  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  changeAuthState = authState => event => {
    event.preventDefault();

    this.props.onChangeAuthState(authState);
  };

  handleSubmit = event => {
    debugger;
    event.preventDefault();
    if (this.isSignup) {
      let userReg = {
        name: this.state.name,
        email: this.state.signupEmail,
        password: this.state.signupPassword,
        username: this.state.signupUsername
      };
      //call action
      debugger;
      this.props
        .createTempSignup({
          variables: {
            username: userReg.username,
            email: userReg.email,
            password: userReg.password
          }
        })
        .then(res => {
          if (
            res &&
            res.data &&
            res.data.createTempSignup &&
            res.data.createTempSignup._id
          ) {
            this.setState({
              registrationSuccess: "pass",
              registrationMessage:
                "Registration successfull, Please check your email to verify",
              signupEmail: "",
              signupUsername: "",
              signupPassword: "",
              signupConfirmPassword: ""
            });
          } else {
            this.setState({
              registrationSuccess: "fail",
              registrationMessage:
                "Something wrong in signup, Please try latter"
            });
          }
        })
        .catch(error => {
          debugger;
          if (error && error.graphQLErrors) {
            this.setState({
              registrationSuccess: "fail",
              registrationMessage: error.graphQLErrors[0].message
            });
          } else {
            this.setState({
              registrationSuccess: "fail",
              registrationMessage:
                "Something wrong in signup, Please try latter"
            });
          }
        });
    } else {
      let loginDetail = {
        username: this.state.loginUsername,
        password: this.state.loginPassword
      };
      debugger;
      this.props.signin({
        variables: {
          username: loginDetail.username,
          password: loginDetail.password
        }
      });
    }
  };

  renderButtonText() {
    const { buttonText } = this.props;

    if (!buttonText && this.isLogin) {
      return "Login";
    }

    if (!buttonText && this.isSignup) {
      return "Signup";
    }

    return buttonText;
  }

  render() {
    const {
      showLogo,
      signupEmailLabel,
      signupEmailLabelInputProps,
      signupusernameLabel,
      signupusernameLabelInputProps,
      signupPasswordLabel,
      signupPasswordLabelInputProps,
      loginUsernameLabel,
      loginUsernameLabelInputProps,
      loginPasswordLabel,
      loginPasswordLabelInputProps,
      signupConfirmPasswordLabel,
      signupCconfirmPasswordInputProps,
      children,
      onLogoClick
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        {showLogo && (
          <div className="text-center pb-4">
            <h2 style={{ cursor: "pointer" }} onClick={onLogoClick}>
              SlateStudio
            </h2>
          </div>
        )}
        {this.state.registrationSuccess === "fail" && (
          <Alert color="danger">{this.state.registrationMessage}</Alert>
        )}
        {this.state.registrationSuccess === "pass" && (
          <Alert color="success">{this.state.registrationMessage}</Alert>
        )}
        {this.props.loginSuccess === "fail" && (
          <Alert color="danger">{this.props.loginResponse.message}</Alert>
        )}
        {this.isSignup && (
          <FormGroup>
            <Label for={signupEmailLabel}>{signupEmailLabel}</Label>
            <Input
              {...signupEmailLabelInputProps}
              value={this.state.signupEmail}
              onChange={this.onChange}
              required
            />
          </FormGroup>
        )}
        {this.isSignup && (
          <FormGroup>
            <Label for={signupusernameLabel}>{signupusernameLabel}</Label>
            <Input
              {...signupusernameLabelInputProps}
              value={this.state.signupUsername}
              onChange={this.onChange}
              required
            />
          </FormGroup>
        )}
        {this.isSignup && (
          <FormGroup>
            <Label for={signupPasswordLabel}>{signupPasswordLabel}</Label>
            <Input
              {...signupPasswordLabelInputProps}
              value={this.state.signupPassword}
              onChange={this.onChange}
              required
            />
          </FormGroup>
        )}
        {this.isSignup && (
          <FormGroup>
            <Label for={signupConfirmPasswordLabel}>
              {signupConfirmPasswordLabel}
            </Label>
            <Input
              {...signupCconfirmPasswordInputProps}
              value={this.state.signupConfirmPassword}
              onChange={this.onChange}
              required
            />
          </FormGroup>
        )}
        {!this.isSignup && (
          <FormGroup>
            <Label for={loginUsernameLabel}>{loginUsernameLabel}</Label>
            <Input
              {...loginUsernameLabelInputProps}
              value={this.state.loginUsername}
              onChange={this.onChange}
              required
            />
          </FormGroup>
        )}
        {!this.isSignup && (
          <FormGroup>
            <Label for={loginPasswordLabel}>{loginPasswordLabel}</Label>
            <Input
              {...loginPasswordLabelInputProps}
              value={this.state.loginPassword}
              onChange={this.onChange}
              required
            />
          </FormGroup>
        )}
        <FormGroup check>
          <Label check>
            <Input type="checkbox" />{" "}
            {this.isSignup ? "Agree the terms and policy" : "Remember me"}
          </Label>
        </FormGroup>
        <hr />
        <Button size="lg" className="bg-gradient-theme-left border-0" block>
          {this.renderButtonText()}
        </Button>
        <div className="text-left pt-1">
          <h6>
            {this.isSignup ? (
              <a href="/login" onClick={this.changeAuthState(STATE_LOGIN)}>
                I already have a membership
              </a>
            ) : (
              <div>
                <p>
                  <a
                    href="/signup"
                    onClick={this.changeAuthState(STATE_SIGNUP)}
                  >
                    I forgot my password
                  </a>
                </p>
                <p>
                  <a
                    href="/signup"
                    onClick={this.changeAuthState(STATE_SIGNUP)}
                  >
                    I do not have membership
                  </a>
                </p>
              </div>
            )}
          </h6>
        </div>
        {children}
      </Form>
    );
  }
}

export const STATE_LOGIN = "LOGIN";
export const STATE_SIGNUP = "SIGNUP";

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP]).isRequired,
  showLogo: PropTypes.bool,
  signupEmailLabel: PropTypes.string,
  signupEmailInputProps: PropTypes.object,
  signupusernameLabel: PropTypes.string,
  signupusernameLabelInputProps: PropTypes.object,
  signupPasswordLabel: PropTypes.string,
  signupPasswordLabelInputProps: PropTypes.object,
  signupConfirmPasswordLabel: PropTypes.string,
  signupCconfirmPasswordInputProps: PropTypes.object,
  loginUsernameLabel: PropTypes.string,
  loginUsernameLabelInputProps: PropTypes.object,
  loginPasswordLabel: PropTypes.string,
  loginPasswordLabelInputProps: PropTypes.object,
  onLogoClick: PropTypes.func
};

AuthForm.defaultProps = {
  authState: "LOGIN",
  showLogo: true,
  signupEmailLabel: "Email",
  signupEmailLabelInputProps: {
    type: "email",
    placeholder: "your@email.com",
    name: "signupEmail"
  },
  signupusernameLabel: "Username",
  signupusernameLabelInputProps: {
    type: "text",
    placeholder: "Username must unique",
    name: "signupUsername"
  },
  signupPasswordLabel: "Password",
  signupPasswordLabelInputProps: {
    type: "password",
    placeholder: "your password",
    name: "signupPassword"
  },
  signupConfirmPasswordLabel: "Confirm Password",
  signupCconfirmPasswordInputProps: {
    type: "password",
    placeholder: "confirm your password",
    name: "signupConfirmPassword"
  },
  loginUsernameLabel: "Username",
  loginUsernameLabelInputProps: {
    type: "text",
    placeholder: "Enter your registered username",
    name: "loginUsername"
  },
  loginPasswordLabel: "Password",
  loginPasswordLabelInputProps: {
    type: "password",
    placeholder: "your password",
    name: "loginPassword"
  },
  onLogoClick: () => {}
};

// AuthForm.propTypes = {
//   doRegister: propTypes.func.isRequired
// };

// const mapStateToProps = (state, props) => {
//   return {
//     register: state.register.response,
//     registrationSuccess: state.register.success,
//     loginSuccess: state.auth.loginSuccess,
//     loginResponse: state.auth.loginResponse,
//     isAuthenticated: state.auth.isAuthenticated,
//     userDetail: state.auth.userDetail
//   };
// };
export default compose(graphql(createTempSignup, { name: "createTempSignup" }))(
  AuthForm
);
