import { gql } from "apollo-boost";

export const createIncident = gql`
  mutation createIncident(
    $title: String!
    $description: String!
    $type: String!
    $status: String!
    $priority: String!
    $assignedTo: String!
  ) {
    createIncident(
      incidentInput: {
        title: $title
        description: $description
        type: $type
        status: $status
        priority: $priority
        assignedTo: $assignedTo
      }
    ) {
      title
    }
  }
`;

export const getIncidentList = gql`
  query getIncidentList($status: String!, $assignedTo: String!) {
    getIncidentList(status: $status, assignedTo: $assignedTo) {
      _id
      title
      description
      status
      priority
      assignedTo
    }
  }
`;
export const getIncident = gql`
  query getIncident($id: String!) {
    getIncident(id: $id) {
      _id
      title
      description
      status
      priority
      assignedTo
    }
  }
`;

export const updateIncident = gql`
  mutation updateIncident($id: String!, $status: String, $actStatus: String) {
    updateIncident(
      incidentUpdate: { id: $id, status: $status, actStatus: $actStatus }
    ) {
      title
    }
  }
`;
