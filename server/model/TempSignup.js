const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TempSignup = new Schema({
  username: { type: String, required: true },
  email: { type: String, required: true },
  //name: {type:String, required: true},
  name: { type: String },
  password: { type: String, required: true }, //hash created from password
  verified: { type: String, default: "N" },
  verificationKey: String,
  requestorIPAddress: String,
  verifiedIPAddress: String,
  verificationTime: Date,
  created_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("TempSignup", TempSignup);
