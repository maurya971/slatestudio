const UserDetail = require("../../model/UserDetail");

module.exports = {
  users: async (arge, req) => {
    try {
      const tempUsers = await UserDetail.find();
      console.log(tempUsers);
      return tempUsers.map(user => {
        return {
          ...user._doc,
          _id: user.id
        };
      });
    } catch (err) {
      throw err;
    }
  }
};
