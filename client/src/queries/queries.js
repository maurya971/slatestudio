import { gql } from "apollo-boost";

export const createTempSignup = gql`
  mutation createTempSignup(
    $email: String!
    $username: String!
    $password: String!
  ) {
    createTempSignup(
      userInput: { email: $email, username: $username, password: $password }
    ) {
      _id
    }
  }
`;

export const getloginDetail = gql`
  query getloginDetail($username: String, $password: String) {
    getloginDetail(username: $username, password: $password) {
      username
      token
      tokenExpiration
      type
    }
  }
`;

export const createUserDetail = gql`
  mutation createUserDetail(
    $email: String!
    $username: String!
    $name: String!
    $clientId: String!
  ) {
    createUserDetail(
      userInput: {
        email: $email
        username: $username
        name: $name
        clientId: $clientId
      }
    ) {
      _id
    }
  }
`;
