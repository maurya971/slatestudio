/**
 * @author ashutoshmaurya
 * This class provice all service related to logging
 * Log type is INFO, ERROR
 */

exports.logInfo = function(message) {
  console.log("INFO: " + message);
};

exports.logError = function(message, error) {
  console.log("ERROR: " + message);
  console.log(error);
};

exports.logData = function(message, data) {
  console.log("Data: " + message);
  console.log(data);
};
