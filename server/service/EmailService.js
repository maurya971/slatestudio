/**
 * @author ashutoshmaurya
 * This class email service
 * Log type is INFO, ERROR
 */

var nodemailer = require("nodemailer");
var hbs = require("nodemailer-express-handlebars");
var path = require("path");

let config = require("../config");

exports.sendEmailMessage = function(message) {
  let smtpObj = config.SMTPDetails;

  var smtpTransport = nodemailer.createTransport({
    service: smtpObj.host,
    auth: {
      user: smtpObj.username,
      pass: smtpObj.password
    }
  });
  var mailOptions = {
    from: smtpObj.name, // sender address
    to: message.to,
    subject: message.subject
    //html:    message.message
  };

  if (message.useTemplate) {
    mailOptions.template = message.template;
    mailOptions.context = message.context;
    smtpTransport.use(
      "compile",
      hbs({
        viewPath: path.resolve(__dirname, "../tmpl")
      })
    );
  } else if (message.html) {
    mailOptions.html = message.message;
  } else if (message.text) {
    mailOptions.text = message.text;
  }

  smtpTransport.sendMail(mailOptions, function(error, response) {
    if (error) {
      console.log(error);
      //next(error)
    } else {
      console.log(response);
    }

    //if you don't want to use this transport object anymore, uncomment following line
    smtpTransport.close(); // shut down the connection pool, no more messages
  });
};
