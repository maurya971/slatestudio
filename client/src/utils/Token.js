/**
 * ssst: stale studio secure token
 */
const Token = {
  set: function(key, val) {
    localStorage.setItem(key, JSON.stringify(val));
  },
  get: function(key) {
    var item = localStorage.getItem(key);
    return JSON.parse(item);
  },
  remove: function(key) {
    localStorage.removeItem(key);
  },
  clear: function() {
    localStorage.clear();
  },
  parsedToken: function() {
    let res = {
        success: false,
        data: null,
        message: ""
      },
      tokenArray = [],
      token = this.get("ssst");

    if (!token) {
      res.success = false;
      res.message = "Invalid or No tiken passed";
      return res;
    }
    tokenArray = token.split(".");
    if (tokenArray.length < 2) {
      res.success = false;
      res.message = "Invalid or No tiken passed";
      return res;
    }
    res.success = true;
    res.message = "Token parsed successfully";
    res.data = JSON.parse(window.atob(tokenArray[1]));
    return res;
  }
};

export default Token;
