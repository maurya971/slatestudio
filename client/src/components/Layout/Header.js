import React from "react";

import bn from "../../utils/bemnames";
import { Navbar, Nav, NavItem, NavLink, Button } from "reactstrap";
import { Link } from "react-router-dom";
import { MdClearAll, MdAddBox } from "react-icons/md";
import SearchInput from "../../components/SearchInput";
import AuthContext from "../../context/auth-context";

const bem = bn.create("header");

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  handleSidebarControlButton = event => {
    event.preventDefault();
    event.stopPropagation();

    document.querySelector(".cr-sidebar").classList.toggle("cr-sidebar--open");
  };

  render() {
    return (
      <AuthContext.Consumer>
        {context => (
          <Navbar light expand className={bem.b("bg-white")}>
            <Nav navbar className="mr-2">
              <Button outline onClick={this.handleSidebarControlButton}>
                <MdClearAll size={25} />
              </Button>
            </Nav>
            <NavItem className="d-inline-flex text-secondary">
              <NavLink>
                <Link to="/">slateStudio</Link>
              </NavLink>
            </NavItem>
            <Nav navbar>
              <SearchInput />
            </Nav>

            <Nav navbar className={bem.e("nav-right")}>
              {context.type == "admin" && (
                <NavItem className="d-inline-flex text-secondary">
                  <NavLink>
                    <Link to="/addincident">
                      <MdAddBox
                        size={25}
                        className="text-secondary can-click"
                      />
                    </Link>
                  </NavLink>
                </NavItem>
              )}

              {context.token && (
                <NavItem className="d-inline-flex text-secondary">
                  <NavLink>{context.username}</NavLink>
                </NavItem>
              )}
              {context.token && (
                <NavItem className="d-inline-flex text-secondary">
                  <NavLink href="/">Logout</NavLink>
                </NavItem>
              )}
              {!context.token && (
                <NavItem className="d-inline-flex text-secondary">
                  <NavLink>
                    <Link to="/login">Login</Link>
                  </NavLink>
                </NavItem>
              )}
            </Nav>
          </Navbar>
        )}
      </AuthContext.Consumer>
    );
  }
}

export default Header;
