const graphql = require("graphql");
const { GraphQLObjectType, GraphQLString, GraphQLSchema } = graphql;
const _ = require("lodash");

const TempSignup = require("../model/TempSignup");

let TempSignupData = [
  {
    id: "1",
    name: "One"
  },
  {
    id: "2",
    name: "Two"
  },
  {
    id: "3",
    name: "Three"
  },
  {
    id: "4",
    name: "Four"
  },
  {
    id: "5",
    name: "Five"
  }
];

const TempSignupType = new GraphQLObjectType({
  name: "TempSignup",
  fields: () => ({
    id: { type: GraphQLString },
    name: { type: GraphQLString }
  })
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    tempSignup: {
      type: TempSignupType,
      args: { id: { type: GraphQLString } },
      resolve(parent, args) {
        return _.find(TempSignupData, { id: args.id });
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    doRegisterMutation: {
      type: TempSignupType,
      args: {
        username: { type: GraphQLString },
        email: { type: GraphQLString },
        password: { type: GraphQLString }
      },
      resolve(parent, args) {
        let tempSignupToSave = new TempSignup({
          username: args.username,
          email: args.email,
          password: args.password
        });
        return tempSignupToSave.save();
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});
