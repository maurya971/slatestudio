const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
let config = require("../../config");
const TempSignup = require("../../model/TempSignup");
const UserDetail = require("../../model/UserDetail");
const EmailService = require("../../service/EmailService");

const _sendWelcomeEmail = userDetail => {
  let messageObj = {};
  let baseUrl = config.baseUrl;
  let verificationUrl =
    baseUrl + "/register/completeregistration/" + userDetail._id;
  messageObj.subject = "Welcome to Slate Studio !!!";
  messageObj.to = userDetail.email;
  messageObj.useTemplate = false;
  //messageObj.template = "signup";
  //   messageObj.context = {
  //     verificationUrl: verificationUrl
  //   };
  messageObj.html = true;
  messageObj.message =
    "Thanks for signup, Please click bellow URL to verify your email <br>" +
    verificationUrl;
  EmailService.sendEmailMessage(messageObj);
};

const _sendUserWelcomeEmail = userDetail => {
  let messageObj = {};
  let baseUrl = config.baseUrl;
  messageObj.subject = "Welcome to Slate Studio !!!";
  messageObj.to = userDetail.email;
  messageObj.useTemplate = false;
  //messageObj.template = "signup";
  //   messageObj.context = {
  //     verificationUrl: verificationUrl
  //   };
  messageObj.html = true;
  messageObj.message = `You are added as a user in stale stidio, login with bellow information to see your detail <br>
      URL: ${baseUrl} <br>
      Username: ${userDetail.username} <br>
      Password: Password <br>
      ClientId: ${userDetail.clientId}`;

  EmailService.sendEmailMessage(messageObj);
};

module.exports = {
  TempSignups: async (arge, req) => {
    console.log(req.isAuth);
    // if (!req.isAuth) {
    //   throw new Error("Not Authenticated");
    // }
    try {
      const tempSignups = await TempSignup.find();
      return tempSignups.map(event => {
        return {
          ...event._doc,
          _id: event.id
        };
      });
    } catch (err) {
      throw err;
    }
  },
  createTempSignup: async args => {
    try {
      const existingUserByEmail = await TempSignup.findOne({
        email: args.userInput.email
      });
      if (existingUserByEmail) {
        throw new Error("Email is already taken.");
      }
      const existingUserByUsername = await TempSignup.findOne({
        username: args.userInput.username
      });
      if (existingUserByUsername) {
        throw new Error("Username is already taken.");
      }
      const hashedPassword = await bcrypt.hash(args.userInput.password, 12);

      const tempSignupToSave = new TempSignup({
        email: args.userInput.email,
        username: args.userInput.username,
        password: hashedPassword
      });

      const result = await tempSignupToSave.save();
      console.log(result);
      _sendWelcomeEmail(result);
      return { ...result._doc, password: null, _id: result.id };
    } catch (err) {
      throw err;
    }
  },
  createUserDetail: async args => {
    try {
      const existingUserByEmail = await UserDetail.findOne({
        email: args.userInput.email
      });
      if (existingUserByEmail) {
        throw new Error("Email is already taken.");
      }
      const existingUserByUsername = await UserDetail.findOne({
        username: args.userInput.username
      });
      if (existingUserByUsername) {
        throw new Error("Username is already taken.");
      }
      const hashedPassword = await bcrypt.hash("Password", 12);

      const tempUserToSave = new UserDetail({
        email: args.userInput.email,
        type: "user",
        username: args.userInput.username,
        password: hashedPassword,
        clientId: args.userInput.clientId
      });

      const result = await tempUserToSave.save();
      _sendUserWelcomeEmail(result);
      return { ...result._doc, password: null, _id: result.id };
    } catch (err) {
      throw err;
    }
  },
  getloginDetail: async ({ username, password }) => {
    console.log("Inside getloginDetail");
    if (!username) {
      throw new Error("Username is blank");
    }
    const user = await UserDetail.findOne({ username: username });
    if (!user) {
      throw new Error("User does not exist!");
    }
    const isEqual = await bcrypt.compare(password, user.password);
    if (!isEqual) {
      throw new Error("Password is incorrect!");
    }
    const token = jwt.sign(
      {
        username: user.username,
        email: user.email,
        clientId: user.clientId,
        type: user.type
      },
      "somesupersecretkey",
      {
        expiresIn: "1h"
      }
    );
    return {
      username: user.username,
      token: token,
      tokenExpiration: 1,
      clientId: user.clientId,
      type: user.type
    };
  }
};
