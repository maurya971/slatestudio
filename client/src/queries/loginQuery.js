import { gql } from "apollo-boost";

/*const getloginDetail = gql`
  query getloginDetail($username: String, $password: String) {
    getloginDetail(username: $username, password: $password) {
      token
    }
  }
`;*/

export const getloginDetail = gql`
  query getloginDetail($username: String, $password: String) {
    getloginDetail(username: $username, password: $password) {
      username
    }
  }
`;

export default getloginDetail;
