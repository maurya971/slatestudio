import { gql } from "apollo-boost";

export const getUsers = gql`
  {
    users {
      username
      email
    }
  }
`;

export default getUsers;
