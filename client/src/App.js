import { STATE_LOGIN, STATE_SIGNUP } from "./components/AuthForm";
import GAListener from "./components/GAListener";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

import { EmptyLayout, LayoutRoute, MainLayout } from "./components/Layout";
import AuthPage from "./pages/AuthPage";
import LoginPage from "./pages/LoginPage";
//import DashboardPage from "./pages/DashboardPage";
import Dashboard from "./components/Dashboard";

import React from "react";
//import componentQueries from "react-component-queries";
import { Router, Redirect, Switch } from "react-router-dom";
import history from "./components/History";
import AuthContext from "./context/auth-context";
import "./styles/reduction.css";
import "./styles/App.css";
import AddUser from "./components/AddUser";
import AddIncident from "./components/AddIncident";

import Token from "./utils/Token";
import IncidentDetail from "./components/IncidentDetail";

const client = new ApolloClient({
  uri: "http://localhost:3000/graphql",
  onError: e => {
    console.log(e);
  }
});

class App extends React.Component {
  // state = {
  //   token: null,
  //   username: null
  // };
  constructor(props) {
    super(props);
    let localToken = Token.get("ssst");
    let parshedToken = Token.parsedToken();
    this.state = {
      token: localToken ? localToken : null,
      username: parshedToken.data ? parshedToken.data.username : null,
      clientId: parshedToken.data ? parshedToken.data.clientId : null,
      type: parshedToken.data ? parshedToken.data.type : null
    };
  }
  login = (token, username, tokenExpiration) => {
    this.setState({ token: token, username: username });
  };

  logout = () => {
    this.setState({ token: null, username: null });
  };

  render() {
    return (
      <ApolloProvider client={client}>
        <AuthContext.Provider
          value={{
            token: this.state.token,
            username: this.state.username,
            clientId: this.state.clientId,
            type: this.state.type,
            login: this.login,
            logout: this.logout
          }}
        >
          <Router history={history}>
            <GAListener>
              <Switch>
                <LayoutRoute
                  exact
                  path="/login"
                  layout={EmptyLayout}
                  component={props => <LoginPage {...props} />}
                />
                <LayoutRoute
                  exact
                  path="/signup"
                  layout={EmptyLayout}
                  component={props => (
                    <AuthPage {...props} authState={STATE_SIGNUP} />
                  )}
                />
                <LayoutRoute
                  exact
                  path="/"
                  layout={MainLayout}
                  component={Dashboard}
                />
                <LayoutRoute
                  exact
                  path="/adduser"
                  layout={MainLayout}
                  component={AddUser}
                />
                <LayoutRoute
                  exact
                  path="/addincident"
                  layout={MainLayout}
                  component={AddIncident}
                />
                <LayoutRoute
                  exact
                  path="/incident/:id"
                  layout={MainLayout}
                  component={IncidentDetail}
                />
                <Redirect to="/" />
              </Switch>
            </GAListener>
          </Router>
        </AuthContext.Provider>
      </ApolloProvider>
    );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: "xs" };
  }

  if (576 < width && width < 767) {
    return { breakpoint: "sm" };
  }

  if (768 < width && width < 991) {
    return { breakpoint: "md" };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: "lg" };
  }

  if (width > 1200) {
    return { breakpoint: "xl" };
  }

  return { breakpoint: "xs" };
};

//export default componentQueries(query)(App);
export default App;
