var client = require("../model/Client.js");

/**
 * Adding new client
 */
exports.addClient = function(ClientObj, next) {
  client.count({}, function(err, count) {
    console.log("--------Client count is ", count);
    var newClient = client(ClientObj);
    newClient.clientId = count + 1;
    newClient.save(function(err, client) {
      if (err) {
        console.log(err);
        next(err);
      } else {
        next(null, client);
      }
    });
  });
};
