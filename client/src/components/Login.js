import PropTypes from "prop-types";
import React from "react";
import { Alert } from "reactstrap";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { graphql, compose } from "react-apollo";
import { getloginDetail } from "../queries/queries";
import history from "./History";
import authContext from "../context/auth-context";
import Token from "../utils/Token";
import { Link } from "react-router-dom";
// const doRegisterMutation = gql`
//   mutation {
//     doRegisterMutation(username: "", email: "", password: "")
//   }
// `;

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loginUsername: "",
      loginPassword: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  static contextType = authContext;
  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  changeAuthState = authState => event => {
    event.preventDefault();

    this.props.onChangeAuthState(authState);
  };

  _doLogIn() {
    let loginDetail = {
      username: this.state.loginUsername,
      password: this.state.loginPassword
    };
    this.props.getloginDetail
      .refetch({
        username: loginDetail.username,
        password: loginDetail.password
      })
      .then(res => {
        if (
          res &&
          res.data &&
          res.data.getloginDetail &&
          res.data.getloginDetail.token
        ) {
          debugger;
          this.context.login(
            res.data.getloginDetail.token,
            res.data.getloginDetail.username,
            res.data.getloginDetail.tokenExpiration,
            res.data.getloginDetail.clientId
          );
          Token.set("ssst", res.data.getloginDetail.token);
          history.push("/");
        }
      })
      .catch(error => {});
  }

  handleSubmit = event => {
    event.preventDefault();
    this._doLogIn();
  };
  render() {
    const {
      showLogo,
      loginUsernameLabel,
      loginUsernameLabelInputProps,
      loginPasswordLabel,
      loginPasswordLabelInputProps,
      children,
      onLogoClick
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        {showLogo && (
          <div className="text-center pb-4">
            <h2 style={{ cursor: "pointer" }} onClick={onLogoClick}>
              SlateStudio
            </h2>
          </div>
        )}

        {this.props.loginSuccess === "fail" && (
          <Alert color="danger">{this.props.loginResponse.message}</Alert>
        )}
        <FormGroup>
          <Label for={loginUsernameLabel}>{loginUsernameLabel}</Label>
          <Input
            {...loginUsernameLabelInputProps}
            value={this.state.loginUsername}
            onChange={this.onChange}
            required
          />
        </FormGroup>
        <FormGroup>
          <Label for={loginPasswordLabel}>{loginPasswordLabel}</Label>
          <Input
            {...loginPasswordLabelInputProps}
            value={this.state.loginPassword}
            onChange={this.onChange}
            required
          />
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input type="checkbox" /> Remember me
          </Label>
        </FormGroup>
        <hr />
        <Button size="lg" className="bg-gradient-theme-left border-0" block>
          Login
        </Button>
        <div className="text-left pt-1">
          <h6>
            <div>
              <p>
                <Link to="/signup">I do not have membership</Link>
              </p>
            </div>
          </h6>
        </div>
        {children}
      </Form>
    );
  }
}
Login.propTypes = {
  showLogo: PropTypes.bool,
  signupEmailLabel: PropTypes.string,
  signupEmailInputProps: PropTypes.object,
  signupusernameLabel: PropTypes.string,
  signupusernameLabelInputProps: PropTypes.object,
  signupPasswordLabel: PropTypes.string,
  signupPasswordLabelInputProps: PropTypes.object,
  signupConfirmPasswordLabel: PropTypes.string,
  signupCconfirmPasswordInputProps: PropTypes.object,
  loginUsernameLabel: PropTypes.string,
  loginUsernameLabelInputProps: PropTypes.object,
  loginPasswordLabel: PropTypes.string,
  loginPasswordLabelInputProps: PropTypes.object,
  onLogoClick: PropTypes.func
};

Login.defaultProps = {
  authState: "LOGIN",
  showLogo: true,
  signupEmailLabel: "Email",
  signupEmailLabelInputProps: {
    type: "email",
    placeholder: "your@email.com",
    name: "signupEmail"
  },
  signupusernameLabel: "Username",
  signupusernameLabelInputProps: {
    type: "text",
    placeholder: "Username must unique",
    name: "signupUsername"
  },
  signupPasswordLabel: "Password",
  signupPasswordLabelInputProps: {
    type: "password",
    placeholder: "your password",
    name: "signupPassword"
  },
  signupConfirmPasswordLabel: "Confirm Password",
  signupCconfirmPasswordInputProps: {
    type: "password",
    placeholder: "confirm your password",
    name: "signupConfirmPassword"
  },
  loginUsernameLabel: "Username",
  loginUsernameLabelInputProps: {
    type: "text",
    placeholder: "Enter your registered username",
    name: "loginUsername"
  },
  loginPasswordLabel: "Password",
  loginPasswordLabelInputProps: {
    type: "password",
    placeholder: "your password",
    name: "loginPassword"
  },
  onLogoClick: () => {}
};

export default compose(graphql(getloginDetail, { name: "getloginDetail" }))(
  Login
);
